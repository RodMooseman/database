<?php
require ('../site/conenc.php');
$Qry = "SELECT ID_Oracle FROM bdoracle";
$query= $mysqli->query($Qry);
while ($row = $query->fetch_assoc()){ 
$QryID[] = $row['ID_Oracle'];
}
if(isset($QryID) == false)
{
}
else
{
foreach($QryID as $val){
$sql = "DELETE FROM bdoracle WHERE ID_Oracle=$val";
$mysqli->query($sql);
}
}
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link rel="stylesheet" href="../dist/bootstrap.min.css" type="text/css" media="all">
<link rel="stylesheet" type="text/css" href="http://10.191.135.124/sytleATP.css">
<link href="../dist/jquery.bootgrid.css" rel="stylesheet" />
<script src="../dist/jquery-1.11.1.min.js"></script>
<script src="../dist/bootstrap.min.js"></script>
<script src="../dist/jquery.bootgrid.min.js"></script>
<!--  Estilo para muestra de reportes  -->

<style>
	.row > div{
        margin-bottom: 15px;
    }
	.header{
        min-height: 90px;
    }
    .footer{
        min-height: 60px;
    }
    .header, .footer{
        background: #2f2f2f;
    }
    .sidebar{
        background: #dbdfe5;
    }
    .content{
        background: #f6f6ff;
    }
    .sidebar, .content{
        min-height: 300px;
    }
</style>
<!--  FIN Estilo para muestra de reportes  -->
<style>
.example_a {
color: #fff !important;
text-decoration: none;
background: #0f7ea5;
padding: 10px;
border-radius: 4px;
display: inline-block;
border: none;
transition: all 0.4s ease 0s;
}
.example_a:hover {
background: #434343;
letter-spacing: 0.5px;
-webkit-box-shadow: 0px 5px 40px -10px rgba(0,0,0,0.57);
-moz-box-shadow: 0px 5px 40px -10px rgba(0,0,0,0.57);
box-shadow: 5px 40px -10px rgba(0,0,0,0.57);
transition: all 0.4s ease 0s;
}
</style>
<script>
        function realizaProcesoA(valorCajaA){
        var parametros = {
                "valorCajaA" : valorCajaA
        };
        $.ajax({
                data:  parametros,
                url:   'BDHPOO.php',
                type:  'post',
                beforeSend: function () {
                        $("#resultado").html("Procesando, espere por favor...");
                },
                success:  function (response) {
                        $("#resultado").html(response);
                }
                  });
                }
</script>
</head>
<body>
	<div class="container">
      <div class="">
         
        <h1>Ejecución de Flujo F-HPOO-ATPBDORA-008</h1>
        <div class="col-sm-12">
		<div class="well clearfix">
			<div class="pull-right"><button type="button" class="btn btn-xs btn-primary" id="command-add" data-row-id="0"> 
			<span class="glyphicon glyphicon-plus"></span> Nuevo Registro</button></div>
                        <div class="button_cont"><a class="example_a" href="../../IntegraNorm.php">Regresar</a> &nbsp &nbsp &nbsp &nbsp <a class="example_a" href="../../salir.php">Salir</a></div></div>                            
               <table id="info_grid" class="table table-condensed table-hover table-striped" width="60%" cellspacing="0" data-toggle="bootgrid">
		    <thead>
			<tr>
                        <th data-column-id="ID_Oracle">ID Equipo</th>
			<th data-column-id="IP">IP</th> 
			<th data-column-id="Usuario">Usuario</th>
			<th data-column-id="Password">Password</th>
			<th data-column-id="commands" data-formatter="commands" data-sortable="false">Acciones</th>
			</tr>
		    </thead>
		</table>
    </div>
      </div>
    </div>
	
<div id="add_model" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Agregar Registro</h4>
            </div>
            <div class="modal-body">
                <form method="post" id="frm_add">
		 <input type="hidden" value="add" name="action" id="action">
              
                    <div class="form-group">
                    <label for="ID_Proy" class="control-label"></label>
                    <input type="hidden"  class="form-control"  id="ID_Proy" name="ID_Proy" value="1" readonly="readonly" />
                    </div>
                 
                   <div class="form-group">
                    <label for="IP" class="control-label">IP:</label>
                    <input type="text" class="form-control" id="IP" name="IP"/>
                  </div>
                  <div class="form-group">
                    <label for="Usuario" class="control-label">Usuario:</label>
                    <input type="text" class="form-control" id="Usuario" name="Usuario"/>
                  </div>
		 <div class="form-group">
                    <label for="Password" class="control-label">Password:</label>
                    <input type="password" class="form-control" id="Password" name="Password"/>
                  </div> 
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id="btn_add" class="btn btn-primary">Save</button>
            </div>
            
        </form>
        </div>
    </div>
</div>
<div id="edit_model" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Editar Registro</h4>
            </div>
            <div class="modal-body">
                <form method="post" id="frm_edit">
		    <input type="hidden" value="edit" name="action" id="action">
		    <input type="hidden" value="0" name="edit_id" id="edit_id">
                    
                <div class="form-group">
                    <label for="IP" class="control-label">IP:</label>
                    <input type="text" class="form-control" id="edit_IP" name="edit_IP"/>
                </div>
                <div class="form-group">
                    <label for="Usuario" class="control-label">Usuario:</label>
                    <input type="text" class="form-control" id="edit_Usuario" name="edit_Usuario"/>
                </div>
			 <div class="form-group">
                    <label for="Password" class="control-label">Password:</label>
                    <input type="password" class="form-control" id="edit_Password" name="edit_Password"/>
                  </div>
                                                    
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id="btn_edit" class="btn btn-primary">Save</button>
            </div>
	</form>
        </div>
    </div>
</div>
    <br>
<div class="container">

        <div class="row">
            <div class="col-lg-2">
                <div class="sidebar"></div>
            </div>
            <div class="col-lg-8">
                <div class="content">                                      
                   <br>
                   <?php 
                   /*$QryB = "SELECT IP, Usuario, Password FROM bdoracle";
                   $queryB= $mysqli->query($QryB);
                   $Input = "";
                   while ($rowB = $queryB->fetch_assoc()){ 
                   $QryIP = $rowB['IP'];
                   $QryUs = $rowB['Usuario'];
                   $QryPs = $rowB['Password'];
                   $Input .= $QryIP . "," .$QryUs . "," .$QryPs . "|";
                   }
                   $Input = substr($Input, 0,(strlen($Input) - 1));*/
                  $Input = "A";
                   ?>
                   <input type="hidden" name="caja_texto" id="valorA" value="<?php  echo $Input?>" readonly="readonly"/>
                   <input type="button" href="javascript:;" onclick="realizaProcesoA($('#valorA').val());return false;" value="Generar Reporte PreATP"/>
                   <br/>
                   <span id="resultado"></span>
                   <div class="activity-spinner" id="following-activity" style="display: none;">
                   <img alt="Redirigiendo..." src="img/spinner.gif">
                   </div>
                   <br>
                   <span id="resultado"></span>                   
                </div>
            </div>
            <div class="col-lg-2">
                <div class="sidebar"></div>
            </div>
        </div>

    </div>      
</body>
</html>
<script type="text/javascript">
$( document ).ready(function() {
	var grid = $("#info_grid").bootgrid({
		ajax: true,
		rowSelect: true,
		post: function ()
		 {                  
		   return {                           
                   Proyect: "Oracle"
		 };
		},                                
		url: "responseBD.php",
		formatters: {
		        "commands": function(column, row)
		        {
		          return "<button type=\"button\" class=\"btn btn-xs btn-default command-edit\" data-row-id=\"" + row.ID_Win + "\"><span class=\"glyphicon glyphicon-edit\"></span></button> " + 
		          "<button type=\"button\" class=\"btn btn-xs btn-default command-delete\" data-row-id=\"" + row.ID_Win + "\"><span class=\"glyphicon glyphicon-trash\"></span></button>";
		        }
		    }
   }).on("loaded.rs.jquery.bootgrid", function()
        {
    grid.find(".command-edit").on("click", function(e)
    {
	    var ele =$(this).parent();
	    var g_IP = $(this).parent().siblings(':first').html();
            var g_Usuario = $(this).parent().siblings(':nth-of-type(2)').html();
            console.log(g_IP);
            console.log(g_Usuario);
		$('#edit_model').modal('show');
		    if($(this).data("row-id") > 0) {
                    $('#edit_id').val(ele.siblings(':first').html());
                    $('#edit_IP').val(ele.siblings(':nth-of-type(2)').html());
                    $('#edit_Usuario').val(ele.siblings(':nth-of-type(3)').html());
                    $('#edit_Password').val(ele.siblings(':nth-of-type(4)').html());
		    } else {
		    alert('Now row selected! First select row, then click edit button');
		}        
    }).end().find(".command-delete").on("click", function(e)
    {
	var conf = confirm('Delete' + $(this).data("row-id") + ' items?');
	    alert(conf);
            if(conf){
            $.post('responseBD.php', { id: $(this).data("row-id"), action:'delete'}
            , function(){
		$("#info_grid").bootgrid('reload');
            }); 
                    }
    });
});
function ajaxAction(action) {
	data = $("#frm_"+action).serializeArray();
	$.ajax({
	type: "POST",  
	url: "responseBD.php",  
	data: data,
	dataType: "json",       
	success: function(response)  
	{
	$('#'+action+'_model').modal('hide');
	$("#info_grid").bootgrid('reload');
	}   
	});
	}
	$( "#command-add" ).click(function() {
	$('#add_model').modal('show');
	});
	$( "#btn_add" ).click(function() {
	ajaxAction('add');
	});
	$( "#btn_edit" ).click(function() {
	ajaxAction('edit');
	});
});
</script>
